package main

import (
	// "net/http"
	"fmt"
	"log"
	"os"

	"api/models"
	"api/routes"
	"api/seed"

	"github.com/joho/godotenv"
	"github.com/gin-gonic/gin"
	"github.com/gin-contrib/cors"
	// cors "github.com/rs/cors/wrapper/gin"
)

var server = models.Server{}

func main() {
	// Check .env file
	var err error
	err = godotenv.Load()
	if err != nil {
		log.Fatalf("ENV : ❌ Error getting env, not comming through %v", err)
	} else {
		fmt.Println("ENV : ✔ We are getting the env values \n")
	}


	
	// err := http.ListenAndServe(":3000", app)
	router := gin.Default()
	// router.Use(cors.Default())
	
	// config := cors.DefaultConfig()
  // // config.AllowOrigins = []string{"*"}
  // // config.AllowOrigins = []string{"http://google.com", "http://facebook.com"}
	// config.AllowCredentials =  true
  // config.AllowAllOrigins = true	

	// router.Use(cors.Default())

	router.Use(cors.New(cors.Config{
		AllowMethods:     []string{"GET", "POST", "PATCH", "DELETE", "HEAD", "OPTIONS"},
		AllowHeaders:     []string{"Access-Control-Allow-Headers", "Access-Control-Allow-Origin",  "access-control-allow-origin, access-control-allow-headers", "Content-Type", "X-XSRF-TOKEN", "Accept", "Origin", "X-Requested-With", "Authorization"},
		// ExposeHeaders:    []string{"Content-Length"},
		AllowAllOrigins: true,
		AllowCredentials: false,
		//ExposeHeaders:    []string{"Content-Length"},
		// AllowOriginFunc: func(origin string) bool {
		//   return origin == "https://github.com"
		// },
		// MaxAge: 12 * time.Hour,
	}))
	
	// config := cors.DefaultConfig()
  // // config.AllowOrigins = []string{"*"}
  // // config.AllowOrigins = []string{"http://google.com", "http://facebook.com"}
	// config.AllowCredentials =  true
  // config.AllowAllOrigins = true

	
  // router.Use(cors.Default())
	
	// Connect to database
	server.ConnectDatabase(
		os.Getenv("DB_DRIVER"), 
		os.Getenv("DB_USER"), 
		os.Getenv("DB_PASSWORD"), 
		os.Getenv("DB_PORT"), 
		os.Getenv("DB_HOST"), 
		os.Getenv("DB_NAME"))
	// Routes
	defaultGroup := router.Group("/api")
	routes.Handlers(defaultGroup)
	
	//seed
	seed.CreateTableSeed()
	seed.CreateListsSeed()
	// Run the server
	router.Run(os.Getenv("GOLANG_API_PORT"))

}