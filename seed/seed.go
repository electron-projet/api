package seed

import (
	"api/models"
)

func CreateTableSeed() {

	var tables []models.Table
	models.DB.Find(&tables)
	if len(tables) == 0 {
		Table := models.Table{Title: "Electron project"}
		models.DB.Create(&Table)
	}

}

func CreateListsSeed() {

	var lists []models.List
	models.DB.Preload("Table").Preload("Tasks").Find(&lists)

	ListArray := [4]string{"TODO", "IN PROGRESS", "REVIEW", "DONE"}

	if len(lists) == 0 {
		for i:=0; i<len(ListArray); i++ {
			List := models.List{Title: ListArray[i], TableID: 1}
			models.DB.Create(&List)
		}
	}
}