package models

import (
	"gorm.io/gorm"
	"errors"
)

type Table struct {
	gorm.Model
	Title  		string `json:"title"`
	Lists 		[]List `gorm:"foreignKey:TableID";"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}

func (table *Table) AfterUpdate(tx *gorm.DB) (err error) {
	if err := tx.Model(table).Where("id = ?", table.ID).Preload("Lists.Tasks", "list_id != ?", 1).First(&table).Error; err != nil {
		return errors.New("Unable de to find list")
	}
  return
}