package models

import (
	"fmt"
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	// _ "github.com/jinzhu/gorm/dialects/sqlite"
	// _ "github.com/jinzhu/gorm/dialects/mysql"    //mysql database driver
	// _ "github.com/jinzhu/gorm/dialects/postgres" //postgres database driver
)

var DB *gorm.DB
// var DATABASE_URI string = "golang:golang@tcp(localhost:3306)/books_golang?charset=utf8mb4&parseTime=True&loc=Local"

type Server struct {
	DB     *gorm.DB
}

func (server *Server) ConnectDatabase(Dbdriver, DbUser, DbPassword, DbPort, DbHost, DbName string) {

	var err error

	if Dbdriver == "mysql" {
		DB_URL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)
		server.DB, err = gorm.Open(mysql.Open(DB_URL), &gorm.Config{})
		if err != nil {
			fmt.Printf("DATABASE : ❌ Cannot connect to %s database \n", Dbdriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("DATABASE : ✔ We are connected to the %s database \n", Dbdriver)
		}
	}


	// database, err := gorm.Open(mysql.Open(DATABASE_URI), &gorm.Config{})

	server.DB.AutoMigrate(&Table{}, &List{}, &Task{})

	DB = server.DB
}
