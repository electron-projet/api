package models

import (
	"gorm.io/gorm"
	"errors"
)

type List struct {
	gorm.Model
	Title  		string `json:"title"`
	Tasks 		[]Task `gorm:"foreignKey:ListID";"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	TableID		uint
	Table			Table
}

func (list *List) AfterCreate(tx *gorm.DB) (err error) {
	if err := tx.Model(list).Where("id = ?", list.ID).Preload("Table").First(&list).Error; err != nil {
		return errors.New("Unable de to find list")
	}
  return
}

func (list *List) AfterUpdate(tx *gorm.DB) (err error) {
	if err := tx.Model(list).Where("id = ?", list.ID).Preload("Table").First(&list).Error; err != nil {
		return errors.New("Unable de to find list")
	}
  return
}