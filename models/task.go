package models

import (
	"gorm.io/gorm"
	"errors"
)

type Task struct {
	gorm.Model
	Title  		string  `json:"title"`
	Rank			int 		`json:"rank"`
	Desc			string  `json:"desc"`
	ListID		uint
	List			List
}

func (task *Task) AfterCreate(tx *gorm.DB) (err error) {
	if err := tx.Model(task).Where("id = ?", task.ID).Preload("List.Table").First(&task).Error; err != nil {
		return errors.New("Unable de to find task")
	}
  return
}

func (task *Task) AfterUpdate(tx *gorm.DB) (err error) {
	if err := tx.Model(task).Where("id = ?", task.ID).Preload("List.Table").First(&task).Error; err != nil {
		return errors.New("Unable de to find task")
	}
  return
}