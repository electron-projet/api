package routes

import (
	"api/controllers"
	
	"github.com/gin-gonic/gin"
)

func Handlers(app *gin.RouterGroup) {

	table := app.Group("/tables")
	{
		table.GET("/", controllers.FindTables)
		table.GET("/:id", controllers.FindTable)
		table.POST("/", controllers.CreateTable)
		table.PATCH("/:id", controllers.UpdateTable)
		table.DELETE("/:id", controllers.DeleteTable)
	}	

	lists := app.Group("/lists")
	{
		lists.GET("/", controllers.FindLists)
		lists.GET("/:id", controllers.FindList)
		lists.GET("/table/:id", controllers.FindListByTable)
		lists.POST("/", controllers.CreateList)
		lists.PATCH("/:id", controllers.UpdateList)
		lists.DELETE("/:id", controllers.DeleteList)
	}	

	tasks := app.Group("/tasks")
	{
		tasks.GET("/", controllers.FindTasks)
		tasks.GET("/:id", controllers.FindTask)
		tasks.POST("/", controllers.CreateTask)
		tasks.PATCH("/:id", controllers.UpdateTask)
		tasks.DELETE("/:id", controllers.DeleteTask)
	}	

	// app.GET("/ping", controllers.Ping)
}