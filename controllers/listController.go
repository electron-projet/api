package controllers

import (
	"api/models"
	"net/http"
	// "fmt"

	"github.com/gin-gonic/gin"
)

// GET /Lists
// Find all Lists
func FindLists(c *gin.Context) {
	var lists []models.List
	models.DB.Find(&lists)

	// fmt.Println("test", lists[0].Tasks[0].List.ID)

	c.JSON(http.StatusOK, gin.H{"data": lists})
}

// GET /List/:id
// Find a List
func FindList(c *gin.Context) {
	// Get model if exist
	var list models.List
	if err := models.DB.Where("id = ?", c.Param("id")).Preload("Table").Preload("Tasks").First(&list).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": list})
}

func FindListByTable(c *gin.Context) {
	// Get model if exist
	var lists []models.List
	models.DB.Where("table_id = ?", c.Param("id")).Preload("Table").Find(&lists)
	
	c.JSON(http.StatusOK, gin.H{"result": lists})
}


// POST /list
// Create new list
func CreateList(c *gin.Context) {
	// Validate input
	var input models.List
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create List
	List := models.List{Title: input.Title, TableID: input.TableID, Tasks: input.Tasks}
	// Tasks: input.Tasks
	models.DB.Create(&List)

	c.JSON(http.StatusOK, gin.H{"data": List})
}

// PATCH /list/:id
// Update a list
func UpdateList(c *gin.Context) {
	// Get model if exist
	var list models.List
	if err := models.DB.Where("id = ?", c.Param("id")).First(&list).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input models.List
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	models.DB.Model(&list).Updates(input)

	c.JSON(http.StatusOK, gin.H{"data": list})
}

// DELETE /list/:id
// Delete a list
func DeleteList(c *gin.Context) {
	// Get model if exist
	var list models.List
	if err := models.DB.Where("id = ?", c.Param("id")).First(&list).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	models.DB.Delete(&list)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
