package controllers

import (
	"api/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// GET /Tables
// Find all Tables
func FindTables(c *gin.Context) {
	var tables []models.Table
	models.DB.Preload("Lists.Tasks", func(DB *gorm.DB) *gorm.DB {
			return DB.Order("tasks.rank ASC")
	}).Find(&tables)

	c.JSON(http.StatusOK, gin.H{"result": tables})
}

// GET /Table/:id
// Find a Table
func FindTable(c *gin.Context) {
	// Get model if exist
	var table models.Table
	if err := models.DB.Where("id = ?", c.Param("id")).Preload("Lists").First(&table).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"result": table})
}

// POST /Table
// Create new Table
func CreateTable(c *gin.Context) {
	// Validate input
	var input models.Table
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Table
	Table := models.Table{Title: input.Title}
	models.DB.Create(&Table)

	// var table models.Table
	// if err := models.DB.Where("id = ?", Table.ID).Preload("Lists").First(&table).Error; err != nil {
	// 	c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
	// 	return
	// }

	c.JSON(http.StatusOK, gin.H{"result": Table})
}

// PATCH /Table/:id
// Update a Table
func UpdateTable(c *gin.Context) {
	// Get model if exist
	var table models.Table
	if err := models.DB.Where("id = ?", c.Param("id")).First(&table).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input models.Table
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	models.DB.Model(&table).Updates(input)

	c.JSON(http.StatusOK, gin.H{"result": table})
}

// DELETE /table/:id
// Delete a table
func DeleteTable(c *gin.Context) {
	// Get model if exist
	var table models.Table
	if err := models.DB.Where("id = ?", c.Param("id")).First(&table).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	models.DB.Delete(&table)

	c.JSON(http.StatusOK, gin.H{"result": true})
}
