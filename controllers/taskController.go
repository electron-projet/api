package controllers

import (
	"api/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

// GET /Tasks
// Find all Tasks
func FindTasks(c *gin.Context) {
	var tasks []models.Task
	models.DB.Preload("List.Table").Find(&tasks)

	c.JSON(http.StatusOK, gin.H{"data": tasks})
}

// GET /Tasks/:id
// Find a Task
func FindTask(c *gin.Context) {
	// Get model if exist
	var task models.Task
	if err := models.DB.Where("id = ?", c.Param("id")).Preload("List.Table").First(&task).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": task})
}

// POST /Task
// Create new Task
func CreateTask(c *gin.Context) {
	// Validate input
	var input models.Task
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var tasks []models.Task
	models.DB.Where("list_id = ?", input.ListID).Preload("List.Table").Find(&tasks)
	newRank := input.Rank
	if len(tasks) > 0 {
		lastElement := tasks[len(tasks)-1]
		newRank = lastElement.Rank + 1
	}
	
	// Create Task
	Task := models.Task{Title: input.Title, ListID: input.ListID, Desc: input.Desc, Rank: newRank}
	models.DB.Create(&Task)

	c.JSON(http.StatusOK, gin.H{"data": Task})
}

// PATCH /Tasks/:id
// Update a Task
func UpdateTask(c *gin.Context) {
	// Get model if exist
	var task models.Task
	if err := models.DB.Where("id = ?", c.Param("id")).First(&task).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input models.Task
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	models.DB.Model(&task).Updates(input)

	c.JSON(http.StatusOK, gin.H{"data": task})
}


// DELETE /tasks/:id
// Delete a task
func DeleteTask(c *gin.Context) {
	// Get model if exist
	var task models.Task
	if err := models.DB.Where("id = ?", c.Param("id")).First(&task).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	models.DB.Delete(&task)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
